package test.dto;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.CollectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

/**
 * 两个标签转换
 * @author chenxh
 *
 */
public class ApprTellerConverter implements Converter{

	@Override
	public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {
		ArrayList<ApprTellerDto> list = (ArrayList<ApprTellerDto>) arg0;
//		arg1.startNode("array");
		for (ApprTellerDto apprTellerDto : list) {
			arg1.startNode("ApprTellerArray");
			
			arg1.startNode("authFlag");
			arg1.setValue(apprTellerDto.getApprTellerNo());
			arg1.endNode();
			arg1.endNode();
		}
//		arg1.endNode();
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader arg0, UnmarshallingContext arg1) {
		
		arg0.moveUp();
		arg0.moveUp();
		System.out.println("arg0.getNodeName():"+arg0.getNodeName()+",arg0.hasMoreChildren():"+arg0.hasMoreChildren()+",value:"+arg0.getValue());
		
		System.out.println("ApprTellerConverter:"+arg1.currentObject());
		
		ArrayList<ApprTellerDto> list = new ArrayList<ApprTellerDto>();
		
		arg0.moveDown();
		arg0.moveDown();
		arg0.moveDown();
		ApprTellerDto apprTellerDto = new ApprTellerDto();
		apprTellerDto.setApprTellerNo(arg0.getValue());
		list.add(apprTellerDto);
		
		arg0.moveUp();
		arg0.moveUp();
		arg0.moveUp();
		
		return list;
	}

	@Override
	public boolean canConvert(Class arg0) {
		return true;
	}


}
