package test.dto;

import java.io.Serializable;

public class ApprTellerDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8011443213068566955L;

	private String ApprTellerNo;

	public String getApprTellerNo() {
		return ApprTellerNo;
	}

	public void setApprTellerNo(String apprTellerNo) {
		ApprTellerNo = apprTellerNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApprTellerDto [ApprTellerNo=");
		builder.append(ApprTellerNo);
		builder.append("]");
		return builder.toString();
	}
	
}
