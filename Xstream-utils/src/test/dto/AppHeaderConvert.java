package test.dto;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import test.EsbSubPacketConvert;

import com.singlee.xstream.bean.AliasInfo;
import com.singlee.xstream.bean.AliasTypeEnum;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class AppHeaderConvert implements Converter {
	
	/**
	 * 别名
	 */
	private HashMap<String,String> strAlias = new HashMap<String,String>();
	
	private HashMap<Class<?>,String> classAlias = new HashMap<Class<?>,String>();
	
	private HashMap<String,String> unStrAlias = new HashMap<String,String>();
	
	private HashMap<String,Class<?>> unClassAlias = new HashMap<String,Class<?>>();
	
	private HashMap<String,String> unFieldAlias = new HashMap<String,String>();
	
	public AppHeaderConvert(EsbSubPacketConvert esbSubPacketConvert) {
		super();
		if(null != esbSubPacketConvert){
			
			unClassAlias.put(esbSubPacketConvert.getSubPacketAliasName(), esbSubPacketConvert.getSubPacketClassType());
			
			if(null != esbSubPacketConvert.getAliasInfos()){
				for (AliasInfo aliasInfo : esbSubPacketConvert.getAliasInfos()) {
					if(aliasInfo.getAliasType().equals(AliasTypeEnum.Unmarshal)){
						classAlias.put(aliasInfo.getImplClass(),aliasInfo.getAliasImplClassName());
						unClassAlias.put(aliasInfo.getAliasImplClassName(), aliasInfo.getImplClass());
						strAlias.put(aliasInfo.getFieldName(),aliasInfo.getAliasName());
						unStrAlias.put(aliasInfo.getAliasName(), aliasInfo.getFieldName());
						unFieldAlias.put(aliasInfo.getAliasImplClassName(), aliasInfo.getFieldName());
					}
				}// end for
			}//end if
		}//end if
	}

	@Override
	public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {
		wirte(arg0,arg1,arg2);
	}
	
	@SuppressWarnings("unchecked")
	private void wirte(Object obj,HierarchicalStreamWriter arg1, MarshallingContext arg2){
		try {
			Field [] fields = obj.getClass().getDeclaredFields();
			
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				
				if("serialVersionUID".equalsIgnoreCase(field.getName())){
					continue;
				}
				
				field.setAccessible(true);
				
//				System.out.println("Obj:"+obj.getClass()+",field.getName():"+field.getName());
//				System.out.println("Obj:"+obj.getClass()+"field.getClass():"+field.getType());
//				System.out.println("Obj:"+obj.getClass()+",getCanonicalName():"+field.getType().getCanonicalName());
//				System.out.println("Obj:"+obj.getClass()+",getSimpleName():"+field.getType().getSimpleName());
//				
				if(field.getType().equals(java.lang.String.class) ||
						field.getType().equals(java.math.BigDecimal.class) ||
						field.getType().equals(java.lang.Integer.class) || 
						field.getType().equals(java.lang.Double.class) ||
						field.getType().equals(java.lang.Float.class) ){
					//处理字符串
					//处理数字类型
					if(null != field.get(obj)){
						arg1.startNode(field.getName());
						arg1.setValue(field.get(obj).toString());
						arg1.endNode();
					}
				}else if(field.getType().equals(java.util.Date.class)){
					//处理Date
					if(null != field.get(obj)){
						arg1.startNode(field.getName());
						arg1.setValue(new SimpleDateFormat("yyyy-MM-dd").format(field.get(obj)));
						arg1.endNode();
					}
				}else if(field.getType().equals(java.util.List.class)){
					//处理list集合对象
					String listName = strAlias.get(field.getName());
					arg1.startNode(listName == null || listName.trim().length() == 0 ? field.getName() : listName);
					ArrayList list = (ArrayList) field.get(obj);
					if(null != list){
						for (Object var : list) {
							//处理集合中每一个对象
							String name = classAlias.get(var.getClass());
							arg1.startNode(null == name || name.trim().length() == 0 ? var.getClass().getName():name);
							wirte(var,arg1,arg2);
							arg1.endNode();
						}//end for
					}//end if
					arg1.endNode();
				}else{
					//处理自定义对象
					Object object = field.get(obj);
					if(null != object){
						arg1.startNode(field.getName());
						wirte(object,arg1,arg2);
						arg1.endNode();
					}//end if 
				}//end if else if ....
				
				field.setAccessible(false);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader arg0, UnmarshallingContext arg1) {
		Object obj = null;
		try {
			Class<?> clz = unClassAlias.get(arg0.getNodeName());
			if(null != clz){
				obj = clz.newInstance();
			}
			reader(arg0.getNodeName(),obj,arg0,arg1,null);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	private void reader(String rootName,Object obj,HierarchicalStreamReader arg0,UnmarshallingContext arg1,ArrayList list) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		
		arg0.moveDown();
		System.out.println("arg0.moveDown()");
		if(arg0.hasMoreChildren()){
			if("array".equalsIgnoreCase(arg0.getNodeName())){
				ArrayList arraylist = new ArrayList();
				System.out.println("if array NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
				reader(rootName,obj,arg0,arg1,arraylist);
			}else{
				System.out.println("if other NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
				Field fieldList = obj.getClass().getDeclaredField(unFieldAlias.get(arg0.getNodeName()));
				fieldList.setAccessible(true);
				Class<?> clz = unClassAlias.get(arg0.getNodeName());
				Object var = clz.newInstance();
				reader(rootName,var,arg0,arg1,list);
				list.add(var);
				fieldList.set(obj, list);
			}
		}else{
			System.out.println("else NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
			Field field = obj.getClass().getDeclaredField(arg0.getNodeName());
			field.setAccessible(true);
			field.set(obj, arg0.getValue());
			field.setAccessible(false);
		}//end else
		arg0.moveUp();
		System.out.println("arg0.moveUp()");
		if(arg0.getNodeName().equalsIgnoreCase(rootName) && !arg0.hasMoreChildren()){
			return;
		}else if(arg0.hasMoreChildren()){
			reader(rootName,obj,arg0,arg1,list);
		}
		
	}
	
	private void reader(HierarchicalStreamReader arg0, UnmarshallingContext arg1,Object obj) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		
		Field field = null;
		String name = null;
		Object var = null;
		Class<?> clz = null;
		ArrayList list = null;
		
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		field = obj.getClass().getDeclaredField(arg0.getNodeName());
		field.setAccessible(true);
		field.set(obj, arg0.getValue());
		field.setAccessible(false);
		arg0.moveUp();
		
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		list = new ArrayList();
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		clz = unClassAlias.get(arg0.getNodeName());
		var = clz.newInstance();
		Field fieldList = obj.getClass().getDeclaredField(unFieldAlias.get(arg0.getNodeName()));
		fieldList.setAccessible(true);
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		field = var.getClass().getDeclaredField(arg0.getNodeName());
		field.setAccessible(true);
		field.set(var, arg0.getValue());
		field.setAccessible(false);
		
		list.add(var);
		
		fieldList.set(obj, list);
		fieldList.setAccessible(false);
		
		arg0.moveUp();
		arg0.moveUp();
		arg0.moveUp();
		
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		field = obj.getClass().getDeclaredField(arg0.getNodeName());
		field.setAccessible(true);
		field.set(obj, arg0.getValue());
		field.setAccessible(false);
		arg0.moveUp();
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		list = new ArrayList();
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		clz = unClassAlias.get(arg0.getNodeName());
		var = clz.newInstance();
		fieldList = obj.getClass().getDeclaredField(unFieldAlias.get(arg0.getNodeName()));
		fieldList.setAccessible(true);
		
		arg0.moveDown();
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		field = var.getClass().getDeclaredField(arg0.getNodeName());
		field.setAccessible(true);
		field.set(var, arg0.getValue());
		field.setAccessible(false);
		
		
		list.add(var);
		
		fieldList.set(obj, list);
		fieldList.setAccessible(false);
	
		arg0.moveUp();
		arg0.moveUp();
		arg0.moveUp();
		
		System.out.println("NodeName:"+arg0.getNodeName()+",isSubNode:"+arg0.hasMoreChildren());
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean canConvert(Class arg0) {
		return true;
	}

}
