package test.dto;

import java.util.List;

import test.EsbSubPacketConvert;
import test.EsbSysHeaderPacket;



public class SysHeaderDto extends EsbSysHeaderPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8157697350436867901L;
	
	private String ServiceCode;
	
	private List<SysHeaderRetDto> array;

	public List<SysHeaderRetDto> getArray() {
		return array;
	}

	public void setArray(List<SysHeaderRetDto> array) {
		this.array = array;
	}

	public String getServiceCode() {
		return ServiceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.ServiceCode = serviceCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public EsbSubPacketConvert getSysHeaderConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		
		info.setSubPacketAliasName("SYS_HEADER");
		info.setSubPacketClassType(SysHeaderDto.class);
		info.addAlias("Ret", SysHeaderRetDto.class);
		return info;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SysHeaderDto [ServiceCode=");
		builder.append(ServiceCode);
		builder.append("]");
		return builder.toString();
	}

}
