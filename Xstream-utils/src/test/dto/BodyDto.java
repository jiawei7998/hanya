package test.dto;

import java.util.List;

import test.EsbBodyPacket;
import test.EsbSubPacketConvert;


public class BodyDto extends EsbBodyPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3330115739029090834L;
	
	private String cmitem;
	private String cmsqno;
	private List<BodyListDto> ARRAY;
	
	public List<BodyListDto> getARRAY() {
		return ARRAY;
	}

	public void setARRAY(List<BodyListDto> aRRAY) {
		ARRAY = aRRAY;
	}

	public String getCmitem() {
		return cmitem;
	}

	public void setCmitem(String cmitem) {
		this.cmitem = cmitem;
	}

	public String getCmsqno() {
		return cmsqno;
	}

	public void setCmsqno(String cmsqno) {
		this.cmsqno = cmsqno;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BodyDto [ARRAY=");
		builder.append(ARRAY);
		builder.append(", cmitem=");
		builder.append(cmitem);
		builder.append(", cmsqno=");
		builder.append(cmsqno);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		
		info.setSubPacketAliasName("BODY");
		info.setSubPacketClassType(BodyDto.class);
		info.addAlias("BodyList", BodyListDto.class);
		return info;
	}

}
