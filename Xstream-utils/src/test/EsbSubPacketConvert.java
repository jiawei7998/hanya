package test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.singlee.xstream.bean.AliasInfo;
import com.singlee.xstream.bean.AliasTypeEnum;
import com.thoughtworks.xstream.converters.Converter;

/**
 * 保存当前域的别名、域的类型等信息
 * @author chenxh
 *
 */
public class EsbSubPacketConvert implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7853560422131799267L;

	/**
	 * 当前域的类型
	 */
	private AliasTypeEnum subPacketType;
	
	/**
	 * 当前域的别名
	 */
	private String subPacketAliasName;
	
	/**
	 * 当前域实现类的类型
	 */
	private Class<?> subPacketClassType;
	
	/**
	 * 别名转换
	 */
	private List<AliasInfo> aliasInfos;
	
	/**
	 * 方法重载.对自定义转换器添序例化的映射
	 * @param fieldName
	 * @param definedClass
	 * @param implClass
	 */
	public void addAlias(String fieldName,String aliasName,Class<?> implClass,String aliasImplClassName){
		getAliasInfos().add(new AliasInfo(fieldName,aliasName,implClass,aliasImplClassName,AliasTypeEnum.Unmarshal));
	}
	
	/**
	 * 方法重载.对指定的类属情添加自定义转换器
	 * @param fieldName
	 * @param definedClass
	 * @param implClass
	 */
	public void addAlias(Class<?> definedClass,String fieldName,Converter converter){
		getAliasInfos().add(new AliasInfo(definedClass,fieldName,converter,AliasTypeEnum.ConvertField));
	}
	
	/**
	 * 方法重载.添加接口/抽象类与实现类的关系映射
	 * @param fieldName
	 * @param definedClass
	 * @param implClass
	 */
	public void addAlias(String fieldName,Class<?> definedClass,Class<?> implClass){
		getAliasInfos().add(new AliasInfo(fieldName,definedClass,implClass,AliasTypeEnum.AliasInterfaceClass));
	}
	
	/**
	 * 方法重载.添加指定实现类属性别名
	 * @param fieldName
	 * @param aliasName
	 * @param definedClass
	 */
	public void addAlias(String fieldName,String aliasName,Class<?> definedClass){
		getAliasInfos().add(new AliasInfo(fieldName,aliasName,definedClass,AliasTypeEnum.AliasField));
	}
	
	/**
	 * 方法重载.添加指定实现类别名
	 * @param fieldName
	 * @param aliasName
	 * @param definedClass
	 */
	public void addAlias(String aliasName,Class<?> definedClass){
		getAliasInfos().add(new AliasInfo(aliasName,definedClass,AliasTypeEnum.AliasClass));
	}
	
	/**
	 * 方法重载.过滤实现类中指定的属性
	 * @param definedClass
	 * @param fieldName
	 */
	public void addAlias(Class<?> definedClass,String fieldName){
		getAliasInfos().add(new AliasInfo(definedClass,fieldName,AliasTypeEnum.OmitField));
	}
	
	/**
	 * 方法重载.添加集合属性别名配置
	 * <p>
	 * 使用该功能后LIST属性将没有层次结构
	 * 如: <List>
	 * 		<ListItem>
	 * 		</ListItem>
	 * 	   <List>
	 * 以上格式将会变更为
	 * 	   <List>
	 *     </List>
	 * </p>
	 * @param definedClass
	 * @param listName
	 * @param listAliasName
	 * @param implClass
	 */
	public void addAlias(Class<?> definedClass,String listName,String listAliasName,Class<?> implClass){
		getAliasInfos().add(new AliasInfo(listName,listAliasName,definedClass,implClass,AliasTypeEnum.AliasList));
	}
	
	/**
	 * 方法重载.复制参数中的数据
	 * @param alias
	 */
	public void addAlias(List<AliasInfo> alias){
		getAliasInfos().addAll(alias);
	}
	
	public List<AliasInfo> getAliasInfos() {
		if(null == aliasInfos){
			aliasInfos = new ArrayList<AliasInfo>();
		}
		return aliasInfos;
	}

	public void setAliasInfos(List<AliasInfo> aliasInfos) {
		this.aliasInfos = aliasInfos;
	}

	public Class<?> getSubPacketClassType() {
		return subPacketClassType;
	}

	public void setSubPacketClassType(Class<?> subPacketClassType) {
		this.subPacketClassType = subPacketClassType;
	}

	public AliasTypeEnum getSubPacketType() {
		return subPacketType;
	}

	public void setSubPacketType(AliasTypeEnum subPacketType) {
		this.subPacketType = subPacketType;
	}

	public String getSubPacketAliasName() {
		return subPacketAliasName;
	}

	public void setSubPacketAliasName(String subPacketAliasName) {
		this.subPacketAliasName = subPacketAliasName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EsbSubPacketConvert [aliasInfos=");
		builder.append(aliasInfos);
		builder.append(", subPacketAliasName=");
		builder.append(subPacketAliasName);
		builder.append(", subPacketClassType=");
		builder.append(subPacketClassType);
		builder.append(", subPacketType=");
		builder.append(subPacketType);
		builder.append("]");
		return builder.toString();
	}
	
	
}
