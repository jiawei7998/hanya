package test;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import test.dto.AppHeaderDto;
import test.dto.ApprTellerDto;
import test.dto.AuthTellerDto;
import test.dto.BodyDto;
import test.dto.BodyListDto;
import test.dto.LocalHeaderDto;
import test.dto.SysHeaderDto;
import test.dto.SysHeaderRetDto;

import com.singlee.xstream.utils.XmlUtils;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			toXml();
//			fromXml(toXml());
//			fromXml(warpLine());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String warpLine(){
		String warpLine = "\r\n";
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+warpLine);
		sb.append("<service>"+warpLine);
		sb.append("  <SYS_HEADER>"+warpLine);
		sb.append("    <ServiceCode>080808808</ServiceCode>"+warpLine);
		sb.append("    		<ServiceCode1>"+warpLine);
		sb.append("    			<ServiceCode2>080808808</ServiceCode2>"+warpLine);
		sb.append("    		</ServiceCode1>"+warpLine);
		sb.append("    <array>"+warpLine);
		sb.append("       <Ret>"+warpLine);
		sb.append("          <ReturnCode>000000</ReturnCode>"+warpLine);
		sb.append("          <ReturnMsg>成功</ReturnMsg>"+warpLine);
		sb.append("       </Ret>"+warpLine);
		sb.append("    </array>"+warpLine);
		sb.append("  </SYS_HEADER>"+warpLine);
		sb.append("  <APP_HEADER>"+warpLine);
		sb.append("    <AppCode>87878787</AppCode>"+warpLine);
		sb.append("    <array>"+warpLine);
		sb.append("        <ApprTellerArray>"+warpLine);
		sb.append("           <ApprTellerNo>1</ApprTellerNo>"+warpLine);
		sb.append("        </ApprTellerArray>"+warpLine);
		sb.append("    </array>"+warpLine);
		sb.append("    <AuthFlag>OK</AuthFlag>"+warpLine);
		sb.append("    <array>"+warpLine);
		sb.append("        <AuthTellerArray>"+warpLine);
		sb.append("           <branchCode>9998</branchCode>"+warpLine);
		sb.append("        </AuthTellerArray>"+warpLine);
		sb.append("    </array>"+warpLine);
		sb.append("  </APP_HEADER>"+warpLine);
		sb.append("  <BODY>"+warpLine);
		sb.append("     <cmitem>01</cmitem>"+warpLine);
		sb.append("     <cmsqno>11</cmsqno>"+warpLine);
		sb.append("     <ARRAY>"+warpLine);
		sb.append("        <BodyList>"+warpLine);
		sb.append("           <id>1</id>"+warpLine);
		sb.append("           <name>中文1</name>"+warpLine);
		sb.append("        </BodyList>"+warpLine);
		sb.append("        <BodyList>"+warpLine);
		sb.append("           <id>2</id>"+warpLine);
		sb.append("           <name>测试2</name>"+warpLine);
		sb.append("        </BodyList>"+warpLine);
		sb.append("     </ARRAY>"+warpLine);
		sb.append(" </BODY>"+warpLine);
		sb.append("</service>");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	public static void fromXml(String xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		
		EsbPacket<BodyDto> responseDto = EsbPacket.initEsbPacket(BodyDto.class,SysHeaderDto.class,AppHeaderDto.class,LocalHeaderDto.class,"service");
		
		EsbPacket<BodyDto> retDto = XmlUtils.fromXML(xml, responseDto);
		
		System.out.println("返回对象:"+retDto);
		
		System.out.println("返回对象:"+retDto.getAppHeader(AppHeaderDto.class).getDealDate());
		
		System.out.println("返回对象转XML："+XmlUtils.toXml(retDto));
		
	}
	
	public static String toXml() throws IllegalArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException{
		
		EsbPacket<BodyDto> requestDto = EsbPacket.initEsbPacket(BodyDto.class,SysHeaderDto.class,AppHeaderDto.class,LocalHeaderDto.class,"service");
		BodyDto body = requestDto.getBody();
		body.setCmitem("01");
		body.setCmsqno("11");
		requestDto.setPackge_type("XML");
		List<BodyListDto> bodyList = new ArrayList<BodyListDto>();
		
//		BodyListDto dodyListDto = new BodyListDto();
//		dodyListDto.setId("1");
//		dodyListDto.setName("中文1");
//		bodyList.add(dodyListDto);
//		
//		dodyListDto = new BodyListDto();
//		dodyListDto.setId("2");
//		dodyListDto.setName("测试2");
//		bodyList.add(dodyListDto);
		
		body.setARRAY(bodyList);
		
		SysHeaderDto sysHeader = requestDto.getSysHeader(SysHeaderDto.class);
		System.out.println("sysHeader:"+sysHeader);
		sysHeader.setServiceCode("080808808");
		
		List<SysHeaderRetDto> sysHeaderRetDtos = new ArrayList<SysHeaderRetDto>();
		SysHeaderRetDto sysHeaderRetDto = new SysHeaderRetDto();
		sysHeaderRetDto.setReturnCode("000000");
		sysHeaderRetDto.setReturnMsg("成功");
		
		sysHeaderRetDtos.add(sysHeaderRetDto);
		
		sysHeader.setArray(sysHeaderRetDtos);
		
		AppHeaderDto appHeader = requestDto.getAppHeader(AppHeaderDto.class);
		appHeader.setAppCode("87878787");
		
		List<ApprTellerDto> apprTellerDtos = new ArrayList<ApprTellerDto>();
		ApprTellerDto apprTellerDto = new ApprTellerDto();
		apprTellerDto.setApprTellerNo("1");
		ApprTellerDto apprTellerDto1 = new ApprTellerDto();
		apprTellerDto1.setApprTellerNo("2");
		apprTellerDtos.add(apprTellerDto);
		apprTellerDtos.add(apprTellerDto1);
		appHeader.setApprTellers(apprTellerDtos);
		
		List<AuthTellerDto> authTellerDtos = new ArrayList<AuthTellerDto>();
		AuthTellerDto authTellerDto = new AuthTellerDto();
		authTellerDto.setBranchCode("9998");
		authTellerDtos.add(authTellerDto);
		appHeader.setAuthTellers(authTellerDtos);
		
		appHeader.setAuthFlag("OK");
		
		AuthTellerDto authTellerDto1 = new AuthTellerDto();
		authTellerDto1.setBranchCode("222");
		appHeader.setAuthTeller(authTellerDto1);
		
		appHeader.setCount(new BigDecimal("0.8"));
		appHeader.setDealDate(new Date());
		
		LocalHeaderDto localHeader = requestDto.getLocalHeader(LocalHeaderDto.class);
		localHeader.setBranchId("01");
		
		System.out.println("RequestDto:"+requestDto);
		
		String xml = XmlUtils.toXml(requestDto);
		
		System.out.println("请求的XML报文"+xml);
		
		return xml;
	}
}
