package com.singlee.financial.cfets.fx;

import java.util.List;

import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;
import com.singlee.hessian.annotation.CfetsFx;
import com.singlee.hessian.annotation.CfetsFxContext;

/**
 * 拆借模块业务处理类,发送外币拆借业务对象到Capital Web资金前置系统中
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
@CfetsFx(context = CfetsFxContext.SINGLEE_API, uri = "/imixFxManageServerExporter")
public interface IImixFxManageServer {

	/**
	 * CFETS登出
	 * 
	 * @return
	 */
	boolean startImixSession();

	/**
	 * CFETS登入
	 * 
	 * @return
	 */
	boolean stopImixSession();

	/**
	 * 交易确认接口
	 * 
	 * @param xml
	 * @return
	 */
	String confirmFxTransaction(String xml);
	/**
	 * 根据日期获取外币发送失败的数据
	 * @param date
	 * @return
	 */
	List<TradingFailureBean> getFailureSendFxByDate(String date);
	
	RetBean resendFxRecord(TradingFailureBean bean) throws Exception;
	

	/**
	 * CFETS登出
	 * 
	 * @return
	 */
	boolean getImixSessionState();

	boolean isSessionStarted();
}
